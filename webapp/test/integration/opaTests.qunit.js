/* global QUnit */
QUnit.config.autostart = false;

sap.ui.getCore().attachInit(function () {
	"use strict";

	sap.ui.require([
		"Tutoriales/Walkthrough-11-Pages-Panels/test/integration/AllJourneys"
	], function () {
		QUnit.start();
	});
});